# Convenia

## Avaliação Técnica - Backend

> Esta avaliação consiste em construir uma API de colaboradores de uma empresa. Seu principal meio de atualização é com processamento em massa de arquivo CSV. Utilizando esta base com Laravel 6 com as migrations e seeds já existentes.
> Esperamos avaliar sua melhor implementação para o contexto que estamos propondo.

Que tenha um excelente desenvolvimento :wink:

### User Story

```gherkin
Feature: API de Colaboradores
Como Usuário do sistema de Colaboradores
Gostaria de contar com uma API para manipular algumas informações de meus colaboradores
De maneira que estas informações alimentem meu sistema pessoal
```

### Requisitos

* Rota para criação e atualização de colaboradores, recebendo arquivo CSV
  * em `tests/_data/employees.csv` e `tests/_data/updateemployees.csv`
* Rota para obter todos os colaboradores
* Rota para excluir um colaborador
* Autenticação com JWT para os itens acima
* Todos os colaboradores devem estar atrelados ao usuário logado na API
* O Usuário deve receber um email a cada atualização

### Observações

* Há um teste de API com codeception em `tests/api`. Para executá-lo utilize `./vendor/bin/codecept run api`.
* Por questão de praticidade utilizamos `sqlite`.
* Procure não modificar a estrutura inicial (`tests`, `migrations` e `seeds`). Caso julgue necessário, comente a respeito em seu **Merge Request**.

### Aguardamos seu Merge Request

Convenia :purple_heart:
